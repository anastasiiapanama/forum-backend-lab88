const express = require('express');
const path = require('path');
const multer = require('multer');

const Post = require('../models/Post');

const {nanoid} = require('nanoid');
const router = express.Router();
const config = require('../config');
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
    destination: (req, file, cb) => { // cb = callback
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
   try {
       const criteria = {};

       if(req.query.user) {
           criteria.user = req.query.user;
       }

       const posts = await Post.find().sort({datetime: 1}).populate('user', '_id username');

       res.send(posts);
   } catch (e) {
       res.sendStatus(500);
   }
});

router.get('/:id', async (req, res) => {
    try {
        const post = await Post.findOne({_id: req.params.id});

        if (post) {
            res.send(post);
        } else {
            res.sendStatus(404);
        }

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        const postRequestData = req.body;
        postRequestData.user = req.user._id;

        if (req.file) {
            postRequestData.image = 'uploads/' + req.file.filename;
        }

        const post = new Post(postRequestData);

        await post.save();

        res.send(post);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;