const express = require('express');
const Comment = require('../models/Comment');
const auth = require("../middleware/auth");
const router = express.Router();

router.get('/', async (req, res) => {
   try {
       const postId = {};

       if (req.query.postId) {
           postId.postId = req.query.postId;
       }

       const userId = {};

       if (req.query.user) {
           userId.user = req.query.user;
       }

       const comments = await Comment.find(postId, userId).populate('postId', '_id').populate('user', '_id username');

       res.send(comments);
   } catch (e) {
       console.log(e);
   }
});

router.post('/', auth, async (req, res) => {
   try {
       const commentReqBody = req.body;
       commentReqBody.user = req.user._id;

       const comment = new Comment(commentReqBody);

       await comment.save();

       res.send(comment);
   } catch (e) {
       console.log(e);
   }
});

module.exports = router;