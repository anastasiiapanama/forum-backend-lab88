const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: function () {
            if (!this.description) {
                return true;
            }
            return false;
        }
    },
    image: {
        type: String,
        required: function () {
            if (!this.image) {
                return true;
            }
            return false;
        }
    },
    datetime: {
        type: String,
        default: new Date().toISOString()
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

const Post = mongoose.model('Post', PostSchema);

module.exports = Post;