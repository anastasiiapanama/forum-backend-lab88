const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');

const Comment = require('./models/Comment');
const Post = require('./models/Post');
const User = require('./models/User');

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    await User.create({
        username: 'admin',
        password: '123',
        token: nanoid()
    }, {
        username: 'user',
        password: '123',
        token: nanoid()
    });

    await mongoose.connection.close();
};

run().catch(console.error);